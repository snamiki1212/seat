let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/js/app.js', 'public/js/build')
    .js('resources/assets/js/library/controls/TrackballControls.js', 'public/js/build')
    .js('resources/assets/js/library/renderers/CSS3DRenderer.js', 'public/js/build')
    .sass('resources/assets/sass/app.scss', 'public/css');
