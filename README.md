# README
==

## Content
SlackAPIとThreeJSのSampleを組み合わせ

## Reference
* [slack api](https://api.slack.com/)
* [threeJs-sample-periodictable](https://threejs.org/examples/#css3d_periodictable)
	* [github](https://github.com/mrdoob/three.js/blob/master/examples/css3d_periodictable.html)

==
