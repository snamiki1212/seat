<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SlackUserProfile extends Model
{
    protected $fillable =
        [
            "first_name",
            "last_name",
            "avatar_hash",
            "real_name",
            "image_24",
            "image_32",
            "image_48",
            "image_72",
            "image_192",
            "image_512",
            "image_1024",
            "image_original",
            "status_emoji",
            "display_name",
            "real_name_normalized",
            "display_name_normalized",
            "team",
        ];

}
