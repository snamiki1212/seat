<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SlackUser extends Model
{
    protected $fillable =
        [
            "id",
            "team_id",
            "name",
            "deleted",
            "color",
            "real_name",
            "tz",
            "tz_label",
            "tz_offset",
            "profile",
            "is_admin",
            "is_owner",
            "is_primary_owner",
            "is_restricted",
            "is_ultra_restricted",
            "is_bot",
            "updated",
            "is_app_user",
            "presence",
        ];
}

