<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    private $x_default  = 5;
    private $y_default  = 1;
    private $wrap_count = 10; // 折り返しを行うx軸の枚数
    private $x;
    private $y;

    public function __construct()
    {
        $this->x = $this->x_default;
        $this->y = $this->y_default;
    }

    public function getX(): int
    {
        if ($this->x % ($this->x_default + $this->wrap_count) == 0) {
            $this->x = $this->x_default;
            $this->y += 1;
        }
        return $this->x++;

    }

    public function getY(): int
    {
        return $this->y;
    }

}
