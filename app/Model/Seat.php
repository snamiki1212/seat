<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
    protected $fillable = [
        "symbol",   // as image_72 in slack at profile
        "detail",   // as name in slack
        "opacity",  //TODO：これどうなってるの？viewだと。
        "x",
        "y",
        "presence"  // same in slack //TODO: away activeでEnum作成
    ];

    public function countProperties():int
    {
        return count($this->fillable);
    }
}



