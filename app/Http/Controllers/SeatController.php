<?php

namespace App\Http\Controllers;

use App\Model\Position;
use App\Model\Seat;
use App\Model\SlackUser;
use App\Model\SlackUserProfile;
use Illuminate\Support\Collection;

class SeatController extends Controller
{
    public function index()
    {
        try {
            $table                    = $this->getSeat();
            $setting["element_count"] = (new Seat())->countProperties();

            return view("welcome", compact(["table", "setting"]));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return Collection
     */
    private function getSeat(): Collection
    {
        $plain_users = $this->getSlackUsersList();
        $users       = $this->convertSlacks2Users($plain_users);
        $seats       = $this->convertUsers2Seats($users);
        $table       = $this->flatSeats4View($seats);
        return $table;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getSlackUsersList(): array
    {
        // Slack APIにて情報取得
        $slackApiKey = config('slack.api-key');
        $url         = "https://slack.com/api/users.list?token=${slackApiKey}&presence=true";
        $content     = file_get_contents($url);
        $result      = json_decode($content, true);
        if (!isset($result["ok"]) || $result["ok"] != true) {
            throw new \Exception("Slackからの情報取得エラー", 500);
        }

        return $result;
    }

    /**
     * @param array $result
     * @return Collection
     */
    public function convertSlacks2Users(array $result): Collection
    {
        // 整形してModelに格納
        return $users = collect($result["members"])
            ->map(function ($member) {
                $profile           = new SlackUserProfile($member["profile"]);
                $member["profile"] = $profile;

                $user = new SlackUser($member);
                return $user;
            })->filter(function ($member) {
                return !(bool)$member->deleted || !(bool)$member->is_bot;
            });
    }

    /**
     * @param Collection $users
     * @return Collection
     */
    private function convertUsers2Seats(Collection $users): Collection
    {
        /** @var  $position x/yのインクリメントを行う加算器 */
        $position = new Position();

        return collect($users)->map(function ($user) use ($position) {
            $entity =
                [
                    "symbol"   => $user->profile->image_72,
                    "detail"   => $user->name,
                    "opacity"  => 0,
                    "x"        => $position->getX(),
                    "y"        => $position->getY(),
                    "presence" => $user->presence,
                ];
            return new Seat($entity);
        });

    }

    /**
     * @param Collection $seats
     * @return Collection
     */
    private function flatSeats4View(Collection $seats): Collection
    {
        //TODO ワンライナー化ができない・・・
        $flatten = collect();
        $seats->each(function ($seat) use (&$flatten) {
            $flatten = $flatten->concat(
                collect($seat->toArray())
                    ->values()
                    ->toArray()
            );
        });
        return $flatten;
    }


    /**
     * テスト用データ流し込み
     *
     * @return array
     */
    private function factorySeat(): array
    {
        return [
            "H", "Hydrogen", "1.1", 1, 1, 0, 0,
            "He", "Helium", "4.002602", 18, 1, 0, 0,
            "Li", "Lithium", "6.941", 1, 2, 1, 0,
            "Be", "Beryllium", "9.012182", 2, 2, 0, 0,
            "B", "Boron", "10.811", 13, 2, 0, 0,
            "C", "Carbon", "12.0107", 14, 2, 0, 0,
            "N", "Nitrogen", "14.0067", 15, 2, 0, 0,
            "O", "Oxygen", "15.9994", 16, 2, 0, 0,
            "F", "Fluorine", "18.9984032", 17, 2, 0, 0,
            "Ne", "Neon", "20.1797", 18, 2, 0, 0,
            "Na", "Sodium", "22.98976...", 1, 3, 0, 0,
            "Mg", "Magnesium", "24.305", 2, 3, 0, 0,
            "Al", "Aluminium", "26.9815386", 13, 3, 0, 0,
            "Si", "Silicon", "28.1855", 14, 3, 0, 0,
            "P", "Phosphorus", "30.973762", 15, 3, 0, 0,
            "S", "Sulfur", "32.065", 16, 3, 0, 0,
            "Cl", "Chlorine", "35.453", 17, 3, 0, 0,
            "Ar", "Argon", "39.948", 18, 3, 0, 0,
            "K", "Potassium", "39.948", 1, 4, 0, 0,
            "Ca", "Calcium", "40.078", 2, 4, 0, 0,
            "Sc", "Scandium", "44.955912", 3, 4, 0, 0,
            "Ti", "Titanium", "47.867", 4, 4, 0, 0,
            "V", "Vanadium", "50.9415", 5, 4, 0, 0,
            "Cr", "Chromium", "51.9961", 6, 4, 0, 0,
            "Mn", "Manganese", "54.938045", 7, 4, 0, 0,
            "Fe", "Iron", "55.845", 8, 4, 0, 0,
            "Co", "Cobalt", "58.933195", 9, 4, 0, 0,
            "Ni", "Nickel", "58.6934", 10, 4, 0, 0,
            "Cu", "Copper", "63.546", 11, 4, 0, 0,
            "Zn", "Zinc", "65.38", 12, 4, 0, 0,
            "Ga", "Gallium", "69.723", 13, 4, 0, 0,
            "Ge", "Germanium", "72.63", 14, 4, 0, 0,
            "As", "Arsenic", "74.9216", 15, 4, 0, 0,
            "Se", "Selenium", "78.96", 16, 4, 0, 0,
            "Br", "Bromine", "79.904", 17, 4, 0, 0,
            "Kr", "Krypton", "83.798", 18, 4, 0, 0,
            "Rb", "Rubidium", "85.4678", 1, 5, 0, 0,
            "Sr", "Strontium", "87.62", 2, 5, 0, 0,
            "Y", "Yttrium", "88.90585", 3, 5, 0, 0,
            "Zr", "Zirconium", "91.224", 4, 5, 0, 0,
            "Nb", "Niobium", "92.90628", 5, 5, 0, 0,
            "Mo", "Molybdenum", "95.96", 6, 5, 0, 0,
            "Tc", "Technetium", "(98)", 7, 5, 0, 0,
            "Ru", "Ruthenium", "101.07", 8, 5, 0, 0,
            "Rh", "Rhodium", "102.9055", 9, 5, 0, 0,
            "Pd", "Palladium", "106.42", 10, 5, 0, 0,
            "Ag", "Silver", "107.8682", 11, 5, 0, 0,
            "Cd", "Cadmium", "112.411", 12, 5, 0, 0,
            "In", "Indium", "114.818", 13, 5, 0, 0,
            "Sn", "Tin", "118.71", 14, 5, 0, 0,
            "Sb", "Antimony", "121.76", 15, 5, 0, 0,
            "Te", "Tellurium", "127.6", 16, 5, 0, 0,
            "I", "Iodine", "126.90447", 17, 5, 0, 0,
            "Xe", "Xenon", "131.293", 18, 5, 0, 0,
            "Cs", "Caesium", "132.9054", 1, 6, 0, 0,
            "Ba", "Barium", "132.9054", 2, 6, 0, 0,
            "La", "Lanthanum", "138.90547", 4, 9, 0, 0,
            "Ce", "Cerium", "140.116", 5, 9, 0, 0,
            "Pr", "Praseodymium", "140.90765", 6, 9, 0, 0,
            "Nd", "Neodymium", "144.242", 7, 9, 0, 0,
            "Pm", "Promethium", "(145)", 8, 9, 0, 0,
            "Sm", "Samarium", "150.36", 9, 9, 0, 0,
            "Eu", "Europium", "151.964", 10, 9, 0, 0,
            "Gd", "Gadolinium", "157.25", 11, 9, 0, 0,
            "Tb", "Terbium", "158.92535", 12, 9, 0, 0,
            "Dy", "Dysprosium", "162.5", 13, 9, 0, 0,
            "Ho", "Holmium", "164.93032", 14, 9, 0, 0,
            "Er", "Erbium", "167.259", 15, 9, 0, 0,
            "Tm", "Thulium", "168.93421", 16, 9, 0, 0,
            "Yb", "Ytterbium", "173.054", 17, 9, 0, 0,
            "Lu", "Lutetium", "174.9668", 18, 9, 0, 0,
            "Hf", "Hafnium", "178.49", 4, 6, 0, 0,
            "Ta", "Tantalum", "180.94788", 5, 6, 0, 0,
            "W", "Tungsten", "183.84", 6, 6, 0, 0,
            "Re", "Rhenium", "186.207", 7, 6, 0, 0,
            "Os", "Osmium", "190.23", 8, 6, 0, 0,
            "Ir", "Iridium", "192.217", 9, 6, 0, 0,
            "Pt", "Platinum", "195.084", 10, 6, 0, 0,
            "Au", "Gold", "196.966569", 11, 6, 0, 0,
            "Hg", "Mercury", "200.59", 12, 6, 0, 0,
            "Tl", "Thallium", "204.3833", 13, 6, 0, 0,
            "Pb", "Lead", "207.2", 14, 6, 0, 0,
            "Bi", "Bismuth", "208.9804", 15, 6, 0, 0,
            "Po", "Polonium", "(209)", 16, 6, 0, 0,
            "At", "Astatine", "(210)", 17, 6, 0, 0,
            "Rn", "Radon", "(222)", 18, 6, 0, 0,
            "Fr", "Francium", "(223)", 1, 7, 0, 0,
            "Ra", "Radium", "(226)", 2, 7, 0, 0,
            "Ac", "Actinium", "(227)", 4, 10, 0, 0,
            "Th", "Thorium", "232.03806", 5, 10, 0, 0,
            "Pa", "Protactinium", "231.0588", 6, 10, 0, 0,
            "U", "Uranium", "238.02891", 7, 10, 0, 0,
            "Np", "Neptunium", "(237)", 8, 10, 0, 0,
            "Pu", "Plutonium", "(244)", 9, 10, 0, 0,
            "Am", "Americium", "(243)", 10, 10, 0, 0,
            "Cm", "Curium", "(247)", 11, 10, 0, 0,
            "Bk", "Berkelium", "(247)", 12, 10, 0, 0,
            "Cf", "Californium", "(251)", 13, 10, 0, 0,
            "Es", "Einstenium", "(252)", 14, 10, 0, 0,
            "Fm", "Fermium", "(257)", 15, 10, 0, 0,
            "Md", "Mendelevium", "(258)", 16, 10, 0, 0,
            "No", "Nobelium", "(259)", 17, 10, 0, 0,
            "Lr", "Lawrencium", "(262)", 18, 10, 0, 0,
            "Rf", "Rutherfordium", "(267)", 4, 7, 0, 0,
            "Db", "Dubnium", "(268)", 5, 7, 0, 0,
            "Sg", "Seaborgium", "(271)", 6, 7, 0, 0,
            "Bh", "Bohrium", "(272)", 7, 7, 0, 0,
            "Hs", "Hassium", "(270)", 8, 7, 0, 0,
            "Mt", "Meitnerium", "(276)", 9, 7, 0, 0,
            "Ds", "Darmstadium", "(281)", 10, 7, 0, 0,
            "Rg", "Roentgenium", "(280)", 11, 7, 0, 0,
            "Cn", "Copernicium", "(285)", 12, 7, 0, 0,
            "Nh", "Nihonium", "(286)", 13, 7, 0, 0,
            "Fl", "Flerovium", "(289)", 14, 7, 0, 0,
            "Mc", "Moscovium", "(290)", 15, 7, 0, 0,
            "Lv", "Livermorium", "(293)", 16, 7, 0, 0,
            "Ts", "Tennessine", "(294)", 17, 7, 0, 0,
            "Og", "Oganesson", "(294)", 18, 70, 0, 0,
        ];
    }
}
