
// 1. symbol name
// 2. name(from slack)
// 3. id
// 4. X
// 5. Y
var table = [];

var camera, scene, renderer;
var controls;

var objects = [];
var targets = { table: [], sphere: [], helix: [], grid: [] };

function getAwayColor(){
    return 'rgba(127,0,0,' + ( Math.random() * 0.5 + 0.25 ) + ')';
}
function getActiveColor(){
    return 'rgba(0,127,127,' + ( Math.random() * 0.5 + 0.25 ) + ')';
}

function init() {

    camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.z = 3000;

    scene = new THREE.Scene();

    // table

    for ( var i = 0; i < table.length; i += 5 ) {

        var element = document.createElement( 'div' );
        element.className = 'element';
        element.id = table[ i + 2 ];
        element.style.backgroundColor = getAwayColor();

        var number = document.createElement( 'div' );
        number.className = 'number';
        number.textContent = (i/5) + 1;
        element.appendChild( number );

        var symbol = document.createElement( 'div' );
        symbol.className = 'symbol';
        symbol.textContent = table[ i ];
        element.appendChild( symbol );

        var details = document.createElement( 'div' );
        details.className = 'details';
        details.innerHTML = table[ i + 1 ] + '<br>' + table[ i + 2 ];
        element.appendChild( details );

        var object = new THREE.CSS3DObject( element );
        object.position.x = Math.random() * 4000 - 2000;
        object.position.y = Math.random() * 4000 - 2000;
        object.position.z = Math.random() * 4000 - 2000;
        scene.add( object );

        objects.push( object );

        //

        var object = new THREE.Object3D();
        object.position.x = ( table[ i + 3 ] * 140 ) - 1330;
        object.position.y = - ( table[ i + 4 ] * 180 ) + 990;

        targets.table.push( object );

    }

    // sphere

    var vector = new THREE.Vector3();
    var spherical = new THREE.Spherical();

    for ( var i = 0, l = objects.length; i < l; i ++ ) {

        var phi = Math.acos( -1 + ( 2 * i ) / l );
        var theta = Math.sqrt( l * Math.PI ) * phi;

        var object = new THREE.Object3D();

        spherical.set( 800, phi, theta );

        object.position.setFromSpherical( spherical );

        vector.copy( object.position ).multiplyScalar( 2 );

        object.lookAt( vector );

        targets.sphere.push( object );

    }

    // helix

    var vector = new THREE.Vector3();
    var cylindrical = new THREE.Cylindrical();

    for ( var i = 0, l = objects.length; i < l; i ++ ) {

        var theta = i * 0.175 + Math.PI;
        var y = - ( i * 8 ) + 450;

        var object = new THREE.Object3D();

        cylindrical.set( 900, theta, y );

        object.position.setFromCylindrical( cylindrical );

        vector.x = object.position.x * 2;
        vector.y = object.position.y;
        vector.z = object.position.z * 2;

        object.lookAt( vector );

        targets.helix.push( object );

    }

    // grid

    for ( var i = 0; i < objects.length; i ++ ) {

        var object = new THREE.Object3D();

        object.position.x = ( ( i % 5 ) * 400 ) - 800;
        object.position.y = ( - ( Math.floor( i / 5 ) % 5 ) * 400 ) + 800;
        object.position.z = ( Math.floor( i / 25 ) ) * 1000 - 2000;

        targets.grid.push( object );

    }

    //

    renderer = new THREE.CSS3DRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.domElement.style.position = 'absolute';
    document.getElementById( 'container' ).appendChild( renderer.domElement );

    //

    controls = new THREE.TrackballControls( camera, renderer.domElement );
    controls.rotateSpeed = 0.5;
    controls.minDistance = 500;
    controls.maxDistance = 6000;
    controls.addEventListener( 'change', render );

    var button = document.getElementById( 'table' );
    button.addEventListener( 'click', function ( event ) {

        transform( targets.table, 2000 );

    }, false );

    var button = document.getElementById( 'sphere' );
    button.addEventListener( 'click', function ( event ) {

        transform( targets.sphere, 2000 );

    }, false );

    var button = document.getElementById( 'helix' );
    button.addEventListener( 'click', function ( event ) {

        transform( targets.helix, 2000 );

    }, false );

    var button = document.getElementById( 'grid' );
    button.addEventListener( 'click', function ( event ) {

        transform( targets.grid, 2000 );

    }, false );

    transform( targets.table, 2000 );

    //

    window.addEventListener( 'resize', onWindowResize, false );

}

function transform( targets, duration ) {

    TWEEN.removeAll();

    for ( var i = 0; i < objects.length; i ++ ) {

        var object = objects[ i ];
        var target = targets[ i ];

        new TWEEN.Tween( object.position )
            .to( { x: target.position.x, y: target.position.y, z: target.position.z }, Math.random() * duration + duration )
            .easing( TWEEN.Easing.Exponential.InOut )
            .start();

        new TWEEN.Tween( object.rotation )
            .to( { x: target.rotation.x, y: target.rotation.y, z: target.rotation.z }, Math.random() * duration + duration )
            .easing( TWEEN.Easing.Exponential.InOut )
            .start();

    }

    new TWEEN.Tween( this )
        .to( {}, duration * 2 )
        .onUpdate( render )
        .start();

}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

    render();

}

function animate() {

    requestAnimationFrame( animate );

    TWEEN.update();

    controls.update();

}

function render() {

    renderer.render( scene, camera );

}

function setSlackStatus() {
    var url = 'https://slack.com/api/users.list';
    var data = {
        token: 'xoxp-23509125269-195233094244-252471296912-a3541b972b41188964dcd9f405309577',
        presence: 'true'
    };

    $.ajax({
        type: 'GET',
        url: url,
        data: data,
    }).done(function (result) {
        console.log('get slack users list!!');
        members = result.members;
        for (var i = 0; i < members.length; i++) {
            slack_id        = $.inArray(members[i].id, table);
            symbol_name_id  = slack_id - 2;
            name_id         = slack_id - 1;

            if (symbol_name_id < 0) {
                continue;
            } else {
                console.log('find!');
                if(members[i].presence == "active") {
                    console.log('and active:' + table[symbol_name_id] + table[slack_id] + table[name_id]);
                    $("#" + table[slack_id]).css({"background-color": getActiveColor()});
                    table[name_id] = members[i].name;

                    // HTML差し替え
                    var details = $('#' + table[slack_id]).children('.details');
                    details.html(table[name_id] + "<br>" + table[slack_id]);
                }
            }
        }
    }).fail(function (result) {
        alert('you cannnot get slack users list.');
    });

}


function getUsers(){
    console.log("call_PHP");

    $.ajax('../php/getUser.php',
        {
            type: 'get',
            dataType: 'json',
            async: 'false'
        }
    )
        .done(function(result) {
            console.log('DB正常取得');
            new_table = [];
            $.each(result, function(index, row){
                symbol = row.symbol;
                slack_name = row.slack_name;
                slack_id = row.slack_id;
                x = row.x ? row.x : rand(2,17); // 適当にx軸
                y = row.y? row.y : rand(0,10);// 適当にy軸
                new_table.push(symbol, slack_name, slack_id, x, y);
            });
            // reset
            table = new_table;
            $(".element").remove();
            init();
            setSlackStatus();
        })
        .fail(function() {
            window.alert('正しい結果を得られませんでした。');
        });
}

function rand(min, max){
    return Math.floor( Math.random() * (max + 1 - min) ) + min ;
}

getUsers();
init();
animate();

