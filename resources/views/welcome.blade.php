<!DOCTYPE html>
<html>
<head>
  <title>three-88.min.js css3d - periodic table</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <style>
    html, body {
      height: 100%;
    }

    body {
      background-color: #000000;
      margin: 0;
      font-family: Helvetica, sans-serif;;
      overflow: hidden;
    }

    a {
      color: #ffffff;
    }

    #info {
      position: absolute;
      width: 100%;
      color: #ffffff;
      padding: 5px;
      font-family: Monospace;
      font-size: 13px;
      font-weight: bold;
      text-align: center;
      z-index: 1;
    }

    #menu {
      position: absolute;
      bottom: 20px;
      width: 100%;
      text-align: center;
    }

    .element {
      width: 120px;
      height: 160px;
      box-shadow: 0px 0px 12px rgba(0, 255, 255, 0.5);
      border: 1px solid rgba(127, 255, 255, 0.25);
      text-align: center;
      cursor: default;
    }

    .element:hover {
      box-shadow: 0px 0px 12px rgba(0, 255, 255, 0.75);
      border: 1px solid rgba(127, 255, 255, 0.75);
    }

    .element .number {
      position: absolute;
      top: 20px;
      right: 20px;
      font-size: 12px;
      color: rgba(127, 255, 255, 0.75);
    }

    .element .symbol {
      position: absolute;
      top: 40px;
      left: 0px;
      right: 0px;
      font-size: 60px;
      font-weight: bold;
      color: rgba(255, 255, 255, 0.75);
      text-shadow: 0 0 10px rgba(0, 255, 255, 0.95);
    }

    .element .details {
      position: absolute;
      bottom: 15px;
      left: 0px;
      right: 0px;
      font-size: 12px;
      color: rgba(127, 255, 255, 0.75);
    }

    button {
      color: rgba(127, 255, 255, 0.75);
      background: transparent;
      outline: 1px solid rgba(127, 255, 255, 0.75);
      border: 0px;
      padding: 5px 10px;
      cursor: pointer;
    }

    button:hover {
      background-color: rgba(0, 255, 255, 0.5);
    }

    button:active {
      color: #000000;
      background-color: rgba(0, 255, 255, 0.75);
    }
  </style>
</head>
<body>
{{-- 基底ライブラリ--}}
<script src={{ asset("js/non-build/jquery-2.2.4.min.js") }}></script>
<script src={{ asset("js/non-build/three-88.min.js") }}></script>
<script src={{ asset("js/non-build/tween-16.3.5.min.js") }}></script>

{{-- 拡張ライブラリ --}}
<script src={{ asset("js/build/TrackballControls.js") }}></script>
<script src={{ asset("js/build/CSS3DRenderer.js") }}></script>

{{-- 自作スクリプト --}}
<script src={{ asset("js/build/app.js") }}></script>


<div id="container"></div>
<div id="info"><a href="http://threejs.org" target="_blank" rel="noopener">three-88.min.js css3d</a> - periodic table.
  <a href="https://plus.google.com/113862800338869870683/posts/QcFk5HrWran" target="_blank" rel="noopener">info</a>.
</div>
<div id="menu">
  <button id="table">TABLE</button>
  <button id="sphere">SPHERE</button>
  <button id="helix">HELIX</button>
  <button id="grid">GRID</button>
</div>

<script>
    var table = {!! json_encode($table) !!};
    console.log(table);

    var camera, scene, renderer;
    var controls;
    var objects = [];
    var targets = {table: [], sphere: [], helix: [], grid: []};

    // ここから初期処理メソッド定義
    var element_count = {!! $setting["element_count"] !!};
    // ここまで初期処理メソッド定義
    console.log(element_count);


    // ここからメソッド定義
    function init() {
        camera            = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 10000);
        camera.position.z = 3000;
        scene             = new THREE.Scene();
        // table
        for (var i = 0; i < table.length; i += element_count) {
            /**
             * 糞にもほどがある設計
             *
             * １セット単位でループする
             * (ex)１セットが３個の要素の場合、$table = [a_x, a_y, a_z,  b_x, b_y, b_z ]
             *   iの遷移は0→3→6となる。
             *
             * tableでは、
             * ・DIV要素の作成
             * ・１セットで１つのオブジェクトを作成
             *
             * 以降の形状（Helix、Girid）では、オブジェクト単位でループを回す。
             *
             */
            var element       = document.createElement('div');
            element.className = 'element';
            if (table[i + 5] == "away") {
                var color = 'rgba(255,   0,   0,' + ( Math.random() * 0.5 + 0.25 ) + ')';
            } else /* active*/ {
                var color = 'rgba(  0, 127, 127,' + ( Math.random() * 0.5 + 0.25 ) + ')';
            }
            element.style.backgroundColor = color;

            var number                    = document.createElement('div');
            number.className              = 'number';
            number.textContent            = (i / element_count) + 1;
            element.appendChild(number);
            var symbol         = document.createElement('div');
            symbol.className   = 'symbol';
            symbol.innerHTML = "<img src=" + table[i] + " alt='no-image'>";
            element.appendChild(symbol);
            var details       = document.createElement('div');
            details.className = 'details';
            details.innerHTML = table[i + 1] + '<br>' + table[i + 2];
            element.appendChild(details);
            var object        = new THREE.CSS3DObject(element);
            object.position.x = Math.random() * 4000 - 2000;
            object.position.y = Math.random() * 4000 - 2000;
            object.position.z = Math.random() * 4000 - 2000;
            scene.add(object);
            objects.push(object);
            //
            var object        = new THREE.Object3D();
            object.position.x = ( table[i + 3] * 140 ) - 1330;
            object.position.y = -( table[i + 4] * 180 ) + 990;
            targets.table.push(object);
        }
        // sphere
        var vector    = new THREE.Vector3();
        var spherical = new THREE.Spherical();
        for (var i = 0, l = objects.length; i < l; i++) {
            var phi    = Math.acos(-1 + ( 2 * i ) / l);
            var theta  = Math.sqrt(l * Math.PI) * phi;
            var object = new THREE.Object3D();
            spherical.set(800, phi, theta);
            object.position.setFromSpherical(spherical);
            vector.copy(object.position).multiplyScalar(2);
            object.lookAt(vector);
            targets.sphere.push(object);
        }
        // helix
        var vector      = new THREE.Vector3();
        var cylindrical = new THREE.Cylindrical();
        for (var i = 0, l = objects.length; i < l; i++) {
            var theta  = i * 0.175 + Math.PI;
            var y      = -( i * 8 ) + 450;
            var object = new THREE.Object3D();
            cylindrical.set(900, theta, y);
            object.position.setFromCylindrical(cylindrical);
            vector.x = object.position.x * 2;
            vector.y = object.position.y;
            vector.z = object.position.z * 2;
            object.lookAt(vector);
            targets.helix.push(object);
        }
        // grid
        for (var i = 0; i < objects.length; i++) {
            var object        = new THREE.Object3D();
            object.position.x = ( ( i % element_count ) * 400 ) - 800;
            object.position.y = ( -( Math.floor(i / element_count) % element_count ) * 400 ) + 800;
            object.position.z = ( Math.floor(i / (element_count * element_count)) ) * 1000 - 2000;
            targets.grid.push(object);
        }
        //
        renderer = new THREE.CSS3DRenderer();
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.domElement.style.position = 'absolute';
        document.getElementById('container').appendChild(renderer.domElement);
        //
        controls             = new THREE.TrackballControls(camera, renderer.domElement);
        controls.rotateSpeed = 0.5;
        controls.minDistance = 500;
        controls.maxDistance = 6000;
        controls.addEventListener('change', render);
        var button = document.getElementById('table');
        button.addEventListener('click', function (event) {
            transform(targets.table, 2000);
        }, false);
        var button = document.getElementById('sphere');
        button.addEventListener('click', function (event) {
            transform(targets.sphere, 2000);
        }, false);
        var button = document.getElementById('helix');
        button.addEventListener('click', function (event) {
            transform(targets.helix, 2000);
        }, false);
        var button = document.getElementById('grid');
        button.addEventListener('click', function (event) {
            transform(targets.grid, 2000);
        }, false);
        transform(targets.table, 2000);
        //
        window.addEventListener('resize', onWindowResize, false);
    }
    function transform(targets, duration) {
        TWEEN.removeAll();
        for (var i = 0; i < objects.length; i++) {
            var object = objects[i];
            var target = targets[i];
            new TWEEN.Tween(object.position)
                .to({
                    x: target.position.x,
                    y: target.position.y,
                    z: target.position.z
                }, Math.random() * duration + duration)
                .easing(TWEEN.Easing.Exponential.InOut)
                .start();
            new TWEEN.Tween(object.rotation)
                .to({
                    x: target.rotation.x,
                    y: target.rotation.y,
                    z: target.rotation.z
                }, Math.random() * duration + duration)
                .easing(TWEEN.Easing.Exponential.InOut)
                .start();
        }
        new TWEEN.Tween(this)
            .to({}, duration * 2)
            .onUpdate(render)
            .start();
    }
    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
        render();
    }
    function animate() {
        requestAnimationFrame(animate);
        TWEEN.update();
        controls.update();
    }
    function render() {
        renderer.render(scene, camera);
    }
    // ここまでメソッド定義


    // ここから処理実行
    init();
    animate();
    // ここまで処理実行

</script>

</body>
</html>